import { createContext } from 'react';
import React, { useReducer } from 'react';

// component will import TestContext and use useContext
export const TestContext = createContext(null);

// custom HOC to Wrap in App component for every cpn can use
export const ContextProvider = (props) => {
  const innitState = {
    page: 1,
    loading: false,
  };
  const reducer = (state, action) => {
    switch (action.type) {
      case 'increment':
        return { page: state.page + 1, loading: true };
      case 'success':
        return { ...state, loading: false };
      case 'decrement':
        return { page: state.page - 1, loading: true };
      default:
        throw new Error();
    }
  };
  const [state, dispatch] = useReducer(reducer, innitState);
  // bind reducer to provider
  return <TestContext.Provider value={{ state, dispatch }}>{props.children}</TestContext.Provider>;
};
