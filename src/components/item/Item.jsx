import React, { useContext } from 'react';
import { TestContext } from '../storeContext';
import SubItem from './SubItem';

function Item(props) {
  const Props = useContext(TestContext);
  console.log(Props);
  const [listImg, setListImg] = React.useState([]);
  React.useEffect(() => {
    fetch(`https://picsum.photos/v2/list?page=${Props.state.page}&limit=6`)
      .then((res) => {
        if (res.status === 200)
          res.json().then((data) => {
            console.log(data);
            Props.dispatch({ type: 'success' });
            setListImg(data);
          });
        else console.log('error ', res.status);
      })
      .catch((err) => console.log(err));
  }, [Props.state.page]);

  return (
    <div style={{ marginLeft: '14%', display: 'flow-root' }}>
      {listImg.length !== 0 && !Props.state.loading ? <SubItem list={listImg} /> : <SubItem />}
    </div>
  );
}

export default Item;
