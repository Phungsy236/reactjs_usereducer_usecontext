import React from 'react';
import './skeleton.css';
const style = {
  width: '25%',
  float: 'left',
  margin: '10px',
  border: '.1px solid lightgray',
  borderRadius: '3px',
  padding: '5px',
  boxShadow: '0 0 10px gray',
};
// const skeleton = {
//   width: '100%',
//   height: '100%',
//   background: 'gray',
//   animation: '$mymove 5s infinite',

//   '@keyframes mymove': {
//     '50%': { backgroundColr: 'blue' },
//   },
// };

function SubItem(props) {
  const listImg = props.list ? props.list : [];
  console.log('list img render');
  return listImg.length > 0
    ? listImg.map((elm, idx) => (
        <div key={idx} className='wrap-img' style={style}>
          <img src={elm.download_url} alt='anh' width='100%' height='300px' />
        </div>
      ))
    : [1, 2, 3, 4, 5, 6].map((elm) => (
        <div key={elm} className='wrap-img' style={{ ...style, height: '300px' }}>
          <div className='skeleton'></div>
        </div>
      ));
}

export default SubItem;
