import React, { useContext } from 'react';
import { TestContext } from '../storeContext';

function Pagination(props) {
  const style = {
    height: '40px',
    boxShadow: '0 0 10px gray',
    margin: '7px',
    backgroundImage: 'linear-gradient(90deg, #E91E63, rgb(33, 150, 243))',
    borderRadius: '3px',
    border: 'none',
    color: 'white',
    fontWeight: 600,
    cursor: 'pointer',
  };

  const Props = useContext(TestContext);
  console.log('from pagi', Props);
  return (
    <div style={{ textAlign: 'center' }}>
      <button
        disabled={Props.state.page === 1 ? true : false}
        style={style}
        onClick={() => {
          Props.dispatch({ type: 'decrement' });
        }}>
        Last View
      </button>

      <button
        style={style}
        onClick={() => {
          Props.dispatch({ type: 'increment' });
        }}>
        Next View
      </button>
    </div>
  );
}

export default Pagination;
