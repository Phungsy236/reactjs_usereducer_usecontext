import React, { useReducer } from 'react';

import './App.css';
import Item from './components/item/Item';
import Pagination from './components/pagination/Pagination';
import Message from './components/message/Message';
import {  ContextProvider } from './components/storeContext';

function App() {
  // const [isHot, setHot] = React.useState(false);
  // const [listItem, setListItem] = React.useState([]);
  // const [page, setPage] = React.useState(1);

  // const [count, dispatch] = React.useReducer(reducer, 0);

  return (
      <ContextProvider>
        <Item />
        <Pagination />
        <Message />
      </ContextProvider>
  );
}

export default App;
